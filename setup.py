import setuptools

long_description = ''
try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except FileNotFoundError:
    pass

setuptools.setup(
     name='kuploads',
     version='0.1.6',
     author="KDanso",
     author_email="dev@kkdanso.com",
     description="Add export/import features to project",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="https://bitbucket.org/kwekud-team/django-kuploads",
     packages=setuptools.find_packages(),
     include_package_data=True,
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
     install_requires=[
        'jsonfield2>=3.0.3',
        'tablib>=1.0.0',
        'django-object-actions>=2.0.0',
         'openpyxl>=3.0.3'
     ]
)
