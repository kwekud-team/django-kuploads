import datetime, xlrd, csv
from operator import eq
# from commons.utils.dates import parse_date


class LoaderException(Exception):
    pass


class Loader(object):
    name = ''
    sheets = {}
    structure = {}
    header_row = None
    content_row = None

    def __init__(self, file_obj, valid_fields=None, *arg, **kwargs):
        self.file_obj = file_obj
        self.valid_fields = valid_fields or {}

    def get_upload_obj(self, **kwargs):
        raise NotImplemented

    def check_valid_columns(self, **kwargs):
        raise NotImplemented

    def get_header_names(self, **kwargs):
        raise NotImplemented

    def get_sheets(self):
        return {}

    def get_file_info(self):
        filename, name_only, extension = '', '', ''
        if self.file_obj:
            filename = self.file_obj.name
            splitted = filename.split('.')
            name_only = '.'.join(splitted[:-1])
            extension = (splitted[-1] if splitted else '')
        return {'filename': filename,
                'name_only': name_only,
                'extension': extension}


class ExcelLoader(Loader):
    format = ['xls', 'xlsx']
    structure = {}
    book = None

    def __init__(self, file_obj, valid_fields=None, *arg, **kwargs):
        self.book = None
        self.sheets = {}
        self.formatting_info = kwargs.get('formatting_info', None)
        self.header_row = kwargs.get('header_row', 0)
        self.content_row = kwargs.get('content_row', 1)
        self.sheet_order = kwargs.get('sheet_order', [])
        super(ExcelLoader, self).__init__(file_obj, valid_fields=valid_fields, *arg, **kwargs)

    def get_upload_obj(self, sheet_index):
        self.book = self.book or self.get_book()
        self.sheets = self.sheets or self.get_sheets()

        sht = self.sheets.get(sheet_index, None)

        if sht:
            return sht['sheet']

    def get_book(self):
        if not self.book:
            self.formatting_info = True
            try:
                # Load excel file into memory
                book = xlrd.open_workbook(self.file_obj,
                                          formatting_info=self.formatting_info)
            except:
                self.file_obj.seek(0)
                book = xlrd.open_workbook(file_contents=self.file_obj.read(),
                                          formatting_info=self.formatting_info)
            self.book = book
        return self.book

    def get_sheets(self):
        if not self.sheets:
            book = self.get_book()
            if book:
                for pos, name in enumerate(book.sheet_names()):
                    if self.sheet_order:
                        try:
                            sort = self.sheet_order.index(pos)
                        except ValueError:
                            sort = pos
                    else:
                        sort = pos

                    sht = book.sheet_by_index(pos)
                    self.sheets[pos] = {'name': name,
                                        'sheet': sht,
                                        'rows': sht.nrows - self.header_row,
                                        'sort': sort}
        return self.sheets

    def get_header_names(self, sheet_index):
        hdr_row = self.read_row(sheet_index, self.header_row)
        return [x.strip() for x in hdr_row]

    def read_row(self, sheet_index, pos):
        if not isinstance(sheet_index, int):
            raise Exception("Sheet index must be an integer")

        if not isinstance(pos, int):
            raise Exception("Position must be an integer")

        xs = []
        try:
            cells = self.get_upload_obj(sheet_index=sheet_index).row(pos)
        except IndexError:
            cells = []

        for x in cells:
            try:
                xs.append(x.value.strip())
            except:
                xs.append(x.value)
        return xs

    def check_valid_columns(self, sheet_index, valid_fields):
        errors = []
        upload_fields = self.get_header_names(sheet_index=sheet_index)

        for pos, x in enumerate(map(eq, upload_fields, valid_fields)):
            if not x:
                try:
                    txt = valid_fields[pos]
                except:
                    txt = ''
                try:
                    current = upload_fields[pos]
                except:
                    current = ''
                errors.append({
                    "pos": pos,
                    "original": txt,
                    "current": current  # TODO: clean text to prevent script injection
                })

        return errors

    def get_datetime(self, value):
        dt = None
        try:
            xl_date = xlrd.xldate_as_tuple(value, self.book.datemode)
            dt = datetime.datetime(*xl_date)
        except:
            pass
            # dt = parse_date(value)

        return dt

    def get_time(self, value):
        try:
            xl_date = xlrd.xldate_as_tuple(value, self.book.datemode)
            return datetime.time(*xl_date[3:])
        except:
            return None

    def unfloatify(self, value):
        # Excel auto attaches ".0" to all numbers, this get annoying when you
        # are storing as strings
        return str(value).replace(".0", "")

    def get_total_rows(self, sheet_index):
        sheets = self.get_sheets()
        if sheets:
            return sheets[sheet_index]['sheet'].nrows - self.header_row
        return 0


class MultiExcelLoader:
    pass


# class CSVLoader(Loader):
#     format = 'csv'
#     fields = []
#
#     def get_upload_obj(self):
#         ifile = None
#         try:
#             ifile = open(self.file_obj, "rb")
#             t_csv = csv.reader(ifile)
#         except:
#             t_csv = csv.reader(self.file_obj)
#         if ifile:
#             ifile.close()
#         self.file_obj.seek(0)
#         return list(t_csv)
#
#     def read_row(self, pos):
#         xs = []
#         for x in self.upload_obj[pos]:
#             xs.append(x.strip())
#         return xs
#
#     def check_valid_columns(self):
#         if self.has_header:
#             # Compare individual header names
#             lower = str.lower
#             a = map(lower, self.get_header_names()) == map(lower, self.fields)
#         else:
#             # Compare column count
#             a = len(self.get_header_names()) == len(self.fields)
#             a = True
#         return a
#
#     def get_total_rows(self):
#         offset = (1 if self.has_header else 0)
#         return len(self.upload_obj) - offset
