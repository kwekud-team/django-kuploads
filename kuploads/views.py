from django.urls import reverse
from django.conf import settings
from django.http import JsonResponse, HttpResponseRedirect
from django.views.generic import View, DetailView
from django.views.generic.edit import FormView
from django.contrib.sites.models import Site
from django.shortcuts import get_object_or_404

from kommons.utils.generic import get_sets
from kuploads.models import UploadSetup, UploadLog, UploadFile
from kuploads.forms import UploadForm
from kuploads.generators.utils import get_exp_imp
from kuploads.signals import kupload_upload_pre, kupload_upload_progress, kupload_upload_post
from kuploads.constants import KuploadsK
from kdash.views import BaseDashView


class ModelExporterView(View):

    def get(self, request, *args, **kwargs):
        up_setup = get_object_or_404(UploadSetup, pk=self.kwargs['up_setup_pk'])
        model_exp = get_exp_imp(request, up_setup)

        return model_exp.exporter.get_export_response(KuploadsK.FORMAT_EXCEL.value,
                                                      ttype=KuploadsK.EXPORT_TYPE_SAMPLE.value,
                                                      queryset=self.get_queryset(model_exp))

    def get_queryset(self, model_exp):
        model_class = model_exp.model
        return model_class.objects.all()


class UploadFormView(FormView, BaseDashView):
    template_name = "kuploads/form.html"
    form_class = UploadForm
    upload_setup = None
    exp_imp = None

    def dispatch(self, request, *args, **kwargs):
        self.initialize()
        return super().dispatch(request, *args, **kwargs)

    def get_up_setup(self):
        up_setup_pk = self.kwargs.get('up_setup_pk', None)
        if up_setup_pk:
            return get_object_or_404(UploadSetup, pk=up_setup_pk)

        site_id = self.kwargs['site_id']
        if not site_id:
            site_id = Site.objects.get_current(self.request).pk
        return UploadSetup.objects.filter(site_id=site_id,
                                          ext_content_str=self.kwargs['ext_content_str'],
                                          slug=self.kwargs['slug']).first()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['upload_setup'] = self.upload_setup
        kwargs['request'] = self.request
        # kwargs['model'] = self.model
        return kwargs

    def get_form_class(self):
        form_class = self.form_class

        if self.exp_imp.importer.form_class:

            class MergeForm(form_class, self.exp_imp.importer.form_class):
                pass

            form_class = MergeForm

        return form_class

    def initialize(self, **kwargs):
        self.upload_setup = self.get_up_setup()
        self.exp_imp = get_exp_imp(self.request, self.upload_setup)
        self.save_referrer()

    def save_referrer(self):
        referrer = self.request.META.get('HTTP_REFERER', None)
        if referrer and self.request.method == 'GET':
            key = f'{KuploadsK.SESSION_PREFIX.value}-{self.upload_setup.pk}'
            self.request.session[key] = referrer

    def form_valid(self, form):
        upload_log = self.create_log(form.cleaned_data)

        self.success_url = reverse('kuploads:progress', args=(upload_log.pk,))

        kupload_upload_pre.send(sender=UploadLog, request=self.request, upload_log=upload_log)
        return super().form_valid(form)

    def create_log(self, form_data):
        extra_data = self.get_extra_data(form_data)

        upload_log = UploadLog.objects.create(
            upload_setup=self.upload_setup,
            batch_size=form_data.get('batch_size', 0),
            created_by=self.request.user,
            modified_by=self.request.user,
            extra={'data': extra_data}
        )
        self.save_log_files(upload_log, form_data)
        return upload_log

    def save_log_files(self, upload_log, form_data):
        # files = self.request.FILES.getlist('files')
        files = form_data['files']
        exp_imp = get_exp_imp(self.request, upload_log.upload_setup)

        for f_dt in files:
            full_fn = f_dt['file'].name
            filename = full_fn.split('/')[-1]

            sheets = exp_imp.importer.get_sheets(f_dt['dataset'])
            dt = {k: v.as_dict() for k, v in sheets.items()}

            UploadFile.objects.update_or_create(
                upload_log=upload_log,
                original_filename=filename,
                defaults={
                    'file': f_dt['file'],
                    'sheets': dt,
                })

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'upload_setup': self.upload_setup,
            'based_template': settings.KUPLOAD_BASE_TEMPLATE,
            # 'importer_form': self.get_importer_form()
        })
        return context

    def get_extra_data(self, form_data):
        dt = {}
        if self.exp_imp.importer.form_class:
            dt = {x: form_data[x] for x in self.exp_imp.importer.form_class.declared_fields}
        return dt


# class UploadVerifyView(TemplateView):
#     template_name = "uploads/verify.html"
#
#     def initialize(self, **kwargs):
#         self.request = kwargs['request']
#         self.workspace = self.request.workspace
#         self.upload_log = get_workspace_object_or_404(self.workspace, UploadLog,
#                                                       pk=kwargs['pk'])
#
#     def get(self, request, *args, **kwargs):
#         self.initialize(request=request, **kwargs)
#         return super(UploadVerifyView, self).get(request, **kwargs)
#
#     def post(self, request, **kwargs):
#         self.initialize(request=request, **kwargs)
#         proceed_anyway = request.POST.get('proceed_anyway', False)
#         re_upload = request.POST.get('re_upload', False)
#         if proceed_anyway:
#             url = reverse('uploads:progress', args=(self.upload_log.pk,))
#         else:
#             url = reverse('uploads:main',
#                           args=(self.upload_log.upload_setup.parent.pk,
#                                 self.upload_log.pk,))
#         return HttpResponseRedirect(url)
#
#     def get_context_data(self, **kwargs):
#         context = super(UploadVerifyView, self).get_context_data(**kwargs)
#         context.update({'upload_log': self.upload_log })
#         return context


class UploadProgressView(DetailView, BaseDashView):
    template_name = "kuploads/progress.html"
    model = UploadLog
    object = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'based_template': settings.KUPLOAD_BASE_TEMPLATE,
            'sheet_info': self.get_sheet_info(),
            'object': self.get_object()
        })

        return context

    def get_sheet_info(self):
        obj = self.get_object()
        xs = []
        for file in obj.uploadfile_set.all():
            tmp = []
            for key, sheet in file.sheets.items():
                sets = get_sets(sheet['row_count'], obj.batch_size)
                sheet.update({'set_numbers': len(sets)})
                tmp.append(sheet)

            xs.append({
                'file': file,
                'sheets': tmp
            })

        return xs


class UploadGeneratorView(View):
    upload_file = None

    def get(self, request, *args, **kwargs):
        sheet_index = kwargs['sheet_index']
        self.upload_file = UploadFile.objects.filter(pk=kwargs['file_pk']).first()
        exp_imp = get_exp_imp(self.request, self.upload_file.upload_log.upload_setup)
        data = {
            KuploadsK.IMPORT_PROGRESS_PERCENT_DISPLAY.value: 0,
            KuploadsK.IMPORT_PROGRESS_PERCENT_VALUE.value: 0,
            KuploadsK.IMPORT_PROGRESS_IS_COMPLETED.value: False
        }

        if self.upload_file:
            progress = request.GET.get('progress', '1')

            res = exp_imp.importer.run_import(self.upload_file, self.upload_file.file.read(), sheet_index, progress)
            data.update(res)

            kupload_upload_progress.send(sender=UploadFile, request=self.request, sheet_index=sheet_index,
                                         progress=progress)
        return JsonResponse(data)

    def get_issues(self, signal_resp):
        sig_issues = []
        for c in signal_resp:
            sig_issues += c[1]

        issues = (self.upload_file.issues if self.upload_file.issues else {})

        for x in sig_issues:
            key = x.get('type', '') + '_issues'
            if key not in issues:
                issues[key] = []
            if x not in issues[key]:
                issues[key].append(x)

        return issues


class UploadCompleteView(DetailView, BaseDashView):
    template_name = "kuploads/complete.html"
    model = UploadLog
    object = None

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'based_template': settings.KUPLOAD_BASE_TEMPLATE,
        })

        return context

    def get(self, request, *args, **kwargs):
        referrer = self.get_referrer()

        kupload_upload_post.send(sender=UploadLog, request=self.request, upload_log=self.object, referrer=referrer)

        if referrer:
            return HttpResponseRedirect(referrer)

        return super(UploadCompleteView, self).get(request, *args, **kwargs)

    def get_referrer(self):
        key = f'{KuploadsK.SESSION_PREFIX.value}-{self.object.upload_setup.pk}'
        return self.request.session.get(key, None)

# def upload_complete(request, pk):
#     obj = get_workspace_object_or_404(request.workspace, UploadLog, pk=pk)
#     form = None
#
#     form_class = my_import(obj.upload_setup.complete_form_path)
#     if form_class:
#         if request.method == "POST":
#             form = form_class(request.POST)
#             if form.is_valid():
#                 resp = consume_upload_complete.send(sender=UploadLog, request=request, upload_log=obj, form=form)
#                 url = obj.get_absolute_url()
#                 issues = []
#                 for c in resp:
#                     if c[1]:
#                         url = c[1]
#                 return HttpResponseRedirect(url)
#
#         else:
#             form = form_class()
#
#     return render(request, "uploads/complete.html", {'obj': obj, 'form': form})
#
#
# def json_show_sample_fields(request):
#     data = {'content': '<p>* Select format to view sample columns.</p>'}
#     pk = request.GET.get('upload_pk') or None
#     if pk:
#         obj = UploadSetup.objects.filter(pk=pk).first()
#         if obj:
#             if obj.sample_file:
#                 sample_url = obj.sample_file.url
#                 ext = obj.sample_file.url.split('.')[-1]
#                 sample_name = '%s.%s' % (slugify(obj.name), ext)
#
#                 loader_obj = get_uploaded_obj(request, obj.get_loader(00), sample_url)
#                 sample_headers = loader_obj.get_header_names()
#
#                 content = render_to_string("uploads/includes/sample-fields.html",
#                                            {'sample_headers': sample_headers,
#                                             'sample_url': sample_url,
#                                             'sample_name': sample_name,
#                                             'object': obj})
#                 data = {'content': content}
#             else:
#                 data = {'content': ''}
#     return HttpResponse(json.dumps(data), content_type='application/json')
#
#
