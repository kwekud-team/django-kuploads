from enum import Enum


class KuploadsK(Enum):
    FORMAT_EXCEL = 'xlsx'
    FORMAT_CSV = 'csv'

    EXPORT_TYPE_SAMPLE = 'sample'

    IMPORT_PROGRESS_PERCENT_DISPLAY = 'complete_percent_display'
    IMPORT_PROGRESS_PERCENT_VALUE = 'complete_percent_value'
    IMPORT_PROGRESS_IS_COMPLETED = 'complete_is_completed'

    SESSION_PREFIX = 'kuploads'
