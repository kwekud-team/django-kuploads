from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django_object_actions import DjangoObjectActions

from kommons.utils.http import get_request_site
from kuploads.generators.base import ModelExpImp
from kuploads.models import UploadSetup, UploadLog, UploadFile
from kuploads.attrs import UploadSetupAttr, UploadLogAttr, UploadFileAttr
from kuploads.constants import KuploadsK


@admin.register(UploadSetup)
class UploadSetupAdmin(admin.ModelAdmin):
    list_display = UploadSetupAttr.admin_list_display
    list_filter = UploadSetupAttr.admin_list_filter
    fieldsets = UploadSetupAttr.admin_fieldsets
    # list_editable = ['sort', 'in_menu']
    # mptt_indent_field = "name"
    # form = UploadSetupForm


class UploadFileInline(admin.TabularInline):
    model = UploadFile
    extra = 1


@admin.register(UploadLog)
class UploadLogAdmin(admin.ModelAdmin):
    list_display = UploadLogAttr.admin_list_display
    list_filter = UploadLogAttr.admin_list_filter
    inlines = [UploadFileInline]


@admin.register(UploadFile)
class UploadFileAdmin(admin.ModelAdmin):
    list_display = UploadFileAttr.admin_list_display


class KuploadsAdmin(DjangoObjectActions, admin.ModelAdmin):
    actions = ['export_queryset']
    exp_imp_class = None

    def get_site(self, request):
        return get_request_site(request)

    def get_exporter_class(self, request):
        if self.exp_imp_class:
            return self.exp_imp_class
        else:
            class MyModelExpImp(ModelExpImp):
                model = self.model
                export_fields = self.get_list_display(request)
                ignore_fields = ['sort', 'manage_all', 'manage_only_change', 'manage_only_delete']

            return MyModelExpImp
    
    def get_exp_imp(self, request):
        exp_imp_class = self.get_exporter_class(request)
        site = self.get_site(request)
        return exp_imp_class(request, site)

    def export_queryset(self, request, queryset):
        exp_imp = self.get_exp_imp(request)
        return exp_imp.exporter.get_export_response(KuploadsK.FORMAT_EXCEL.value, queryset=queryset)
    export_queryset.short_description = 'Export selected records'

    def upload_entries(self, request, queryset):
        exp_imp = self.get_exp_imp(request)
        url = reverse('kuploads:upload_form', args=(exp_imp.upload_setup.pk,))
        return HttpResponseRedirect(url)

    upload_entries.label = '<i class="fa fa-upload"></i>&nbsp; Upload entries'
    changelist_actions = ('upload_entries',)