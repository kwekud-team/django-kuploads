import django.dispatch


# consume_upload = django.dispatch.Signal(providing_args=["request", "upload", "loader", "floor", "ceiling"])
# consume_prepare_upload = django.dispatch.Signal(providing_args=["request", "section", "file_obj"])
# consume_upload_complete = django.dispatch.Signal(providing_args=["request", "upload", "upload_log", "form"])

kupload_upload_pre = django.dispatch.Signal()
kupload_upload_progress = django.dispatch.Signal()
kupload_upload_post = django.dispatch.Signal()
