from dataclasses import dataclass


@dataclass
class ImportSheetDataC:
    name: str
    sort: int = 0
    row_count: int = 0

    def as_dict(self):
        return {
            'name': self.name,
            'sort': self.sort,
            'row_count': self.row_count
        }
