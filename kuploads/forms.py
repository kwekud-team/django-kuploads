from django import forms 
from django.urls import reverse_lazy
from django.conf import settings
import json

# from commons import get_model_from_str, intify
# from commons.utils.widgets import ReadOnlyWidget
# from extend.uploads.loaders import LoaderException
from kuploads.models import UploadSetup
from kuploads.generators.utils import get_exp_imp
# from extend.uploads.utils import get_template_related_qs, get_uploaders


BLANK_XS = ( ('', '---'), )


class UploadSetupForm(forms.ModelForm): 
    uploader = forms.ChoiceField(choices=BLANK_XS, required=False) 

    class Meta:
        model = UploadSetup
        fields = '__all__'

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        # self.fields['uploader'].choices = get_uploaders()


class UploadForm(forms.Form):
    batch_size = forms.IntegerField(required=False, initial=50, help_text="Number of items to upload at a time")
    files = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

    def __init__(self, *args, **kwargs):
        self.upload_setup = kwargs.pop('upload_setup', None)
        self.request = kwargs.pop('request', None)
        self.valid_fields = kwargs.pop('valid_fields', [])
        super(UploadForm, self).__init__(*args, **kwargs)

        self.exp_imp = get_exp_imp(self.request, self.upload_setup)

        # self.fields['upload_setup'].queryset = self.upload_setups
        # if self.upload_setups.count() == 1:
        #     self.fields['upload_setup'].initial = self.upload_setups[0].pk

    def clean_files(self):
        files = self.files.getlist('files')
        xs = []
        invalid_files = []
        for f in files:
            dataset = self.exp_imp.importer.get_dataset(f.read())
            if dataset:
                xs.append({
                    'file': f,
                    'dataset': dataset
                })
            else:
                invalid_files.append(f.name)

        if invalid_files:
            raise forms.ValidationError("Invalid file(s) uploaded. %s" % ', '.join(invalid_files))

        return xs

    def clean_upload_file(self):    
        upload_setup = self.cleaned_data['upload_setup']  
        upload_file = self.cleaned_data['upload_file']  
        loader = upload_setup.get_loader()
        try:
            flat_asset = loader(upload_file, self.valid_fields) 
        except LoaderException:
            frmt = upload_setup.get_format_display()
            raise forms.ValidationError('Please select a valid %s file' % frmt)
        
        if not flat_asset.check_valid_columns():
            raise forms.ValidationError("Columns do not match. Please refer to "
                                        "sample file for correct format")

        upload_file.seek(0)
        return upload_file

    # def clean_structure(self):
    #     structure = self.cleaned_data['structure']
    #     upload_setup = self.cleaned_data['upload_setup']
    #     original_structure = upload_setup.get_loader().structure
    #
    #     try:
    #         load_structure = json.loads(structure)
    #     except:
    #         load_structure = None
    #
    #     dict_structure = {}
    #
    #     if load_structure is not None:
    #         dict_structure['header'] = load_structure.get('id_header')
    #         #
    #         dict_structure['related'] = []
    #         for rel in original_structure.get('related', []):
    #             nm = 'id_related_' + rel['content_str'].replace('.', '').lower()
    #             dict_structure['related'].append({
    #                 'content_str': rel['content_str'],
    #                 'filters': rel.get('filters', {}),
    #                 'verbose': rel['verbose'],
    #                 'value': load_structure.get(nm, ''),
    #             })
    #
    #         dict_structure['fields'] = []
    #         for field in original_structure.get('fields', []):
    #             dict_structure['fields'].append({
    #                 'slug': field['slug'],
    #                 'verbose': field['verbose'],
    #                 'value': load_structure.get('id_fields_' + field['slug'], ''),
    #             })
    #         #
    #         dict_structure['columns'] = []
    #         for col in original_structure.get('columns', []):
    #             dict_structure['columns'].append({
    #                 'slug': col['slug'],
    #                 'verbose': col['verbose'],
    #                 'start': load_structure.get('id_columns_start_' + col['slug'], 0),
    #                 'length': load_structure.get('id_columns_length_' + col['slug'], 0),
    #             })
    #
    #     else:
    #         raise forge.ValidationError("Invalid structure")
    #
    #     new_structure = json.dumps(dict_structure)
    #     return new_structure