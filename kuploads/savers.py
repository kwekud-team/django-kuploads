
class BaseUploadSaver:
    issues = []
    columns_required = [
        {'column': 'NAME', 'index': 1},
    ]

    def __init__(self, request, loader_obj, sheet_index, object_defaults={}):
        self.request = request
        self.loader_obj = loader_obj
        self.sheet_index = sheet_index
        self.object_defaults = object_defaults

    def run(self, row_index):
        self.issues = []

        row = self.loader_obj.read_row(self.sheet_index, row_index)
        if row:
            is_valid = self.check_valid(row, row_index)
            if is_valid:
                self.pre_run(row, row_index)
                self.prepare(row, row_index)
                self.post_run(row, row_index)

        return self.issues

    def pre_run(self, row, row_index):
        pass

    def post_run(self, row, row_index):
        pass

    def prepare(self, row, row_index):
        pass

    def check_valid(self, row, row_index):
        # is_empty_row = self.check_required(row, row_index, self.columns_required)
        return False if self.issues else True

    def check_required(self, row, row_index, fields):
        empty_vals = []
        is_empty_row = False
        all_empty = row.count('') == len(row)

        if not all_empty:
            for x in fields:
                value = row[x['index']]
                if value:
                    empty_vals.append(False)
                else:
                    empty_vals.append(True)
                    all_valid = False
                    self.issues.append({
                        'row': row_index,
                        'type': 'empty',
                        'desc': "'%s' is required" % x['column']
                    })

            if empty_vals.count(True) == len(fields):
                # This means all required fields were not entered. This is treated
                # as an empty line and not uploaded
                is_empty_row = True

        return is_empty_row
