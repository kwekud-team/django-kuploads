
class UploadSetupAttr:
    admin_list_display = ("name", "ext_content_str", "slug", "is_active")
    admin_list_filter = ("is_active",)
    admin_fieldsets = [
        ("Basic",
         {"fields": ['site', 'ext_content_str', 'name', 'slug']}),
        ("Extra",
         {"fields": ['class_path', 'uploader', 'sample_file', 'note', 'sort', 'is_active', 'structure']}),
    ]


class UploadLogAttr:
    admin_list_display = ("date_created", "upload_setup", "created_by",)
    admin_list_filter = ("date_created",)


class UploadFileAttr:
    list_display = ("upload_log", "file", "item_count")
    admin_list_display = ("upload_log", "file", "item_count")
