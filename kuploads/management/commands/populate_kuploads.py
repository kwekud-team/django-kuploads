from django.core.management.base import BaseCommand

from django.contrib.sites.models import Site
from kuploads.registry import Register


class Command(BaseCommand):
    help = 'Populates Upload setups table with define items'

    def add_arguments(self, parser):
        parser.add_argument('site_id', type=int)

    def handle(self, *args, **options):
        site_id = options['site_id']
        site = Site.objects.get(id=site_id)
        
        Register.populate(site)
        self.stdout.write(self.style.SUCCESS('Populated upload setups successfully'))
