from django.db import models
from django.conf import settings
from jsonfield import JSONField


class UploadSetup(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    ext_content_str = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=100)
    uploader = models.CharField(max_length=250, null=True, blank=True)
    class_path = models.CharField(max_length=250, null=True, blank=True)
    # form_path = models.CharField(max_length=250, null=True, blank=True)
    sample_file = models.FileField(null=True, blank=True, max_length=250, upload_to="kuploads/uploadsetup/sample_file")
    note = models.CharField(max_length=250, null=True, blank=True)
    sort = models.FloatField(default=1000)
    is_active = models.BooleanField(default=True)
    structure = JSONField(default={}, blank=True)

    def __str__(self):
        return f'{self.pk}: {self.name}'

    class Meta:
        ordering = ('sort',)
        unique_together = ["ext_content_str", "site", 'slug']


class UploadLog(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, on_delete=models.CASCADE,
                                   related_name='%(app_label)s%(class)s_created_by')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, on_delete=models.CASCADE,
                                    related_name='%(app_label)s%(class)s_modified_by')
    upload_setup = models.ForeignKey(UploadSetup, on_delete=models.CASCADE)
    batch_size = models.IntegerField(default=0) 
    row_count = models.IntegerField(default=0)
    sheet_count = models.IntegerField(default=0)
    time_taken = models.IntegerField(default=0)
    return_url = models.CharField(max_length=250, null=True, blank=True)
    extra = JSONField(default={}, blank=True)
    
    def __unicode__(self):
        return '%s %s' % (self.upload_setup, self.date_created)
    
    class Meta:
        ordering = ('-date_created',)


class UploadFile(models.Model):
    upload_log = models.ForeignKey(UploadLog, on_delete=models.CASCADE)
    file = models.FileField("File", upload_to="kuploads/uploadfile/file/")
    original_filename = models.CharField("Filename", max_length=100) 
    item_count = models.IntegerField(default=0)
    time_taken = models.IntegerField(default=0)
    sheets = JSONField(default={}, blank=True)
    has_issues = models.BooleanField(default=False)
    issues = JSONField(default={})

    def __str__(self):
        return self.original_filename


