from django.conf import settings
from kommons.utils.core import dotted_import


def get_uploaders(): 
    custom_uploaders = getattr(settings, 'UPLOAD_LOADERS', [])
    choices = [('', '---')]
    for x in custom_uploaders:
        cls = dotted_import(x)
        if cls:
            choices.append( (x, cls.name) ) 
    return tuple(choices)


