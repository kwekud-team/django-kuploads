from kuploads.models import UploadSetup


_KUPLOADS_ITEMS = []


class Register:

    @staticmethod
    def add(generator):

        if generator not in _KUPLOADS_ITEMS:
            _KUPLOADS_ITEMS.append(generator)

    @staticmethod
    def get_items():
        return _KUPLOADS_ITEMS

    @staticmethod
    def populate(site):
        UploadSetup.objects.filter(site=site).update(is_active=False)
        
        for pos, generator_class in enumerate(_KUPLOADS_ITEMS):
            generator = generator_class()
            generator.create_upload_setup(site)
