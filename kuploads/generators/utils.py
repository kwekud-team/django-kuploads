from kommons.utils.core import dotted_import


def get_exp_imp(request, upload_setup):
    if upload_setup.class_path:
        return dotted_import(upload_setup.class_path)(request, upload_setup.site)
