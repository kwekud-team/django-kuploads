from django import forms


class ModelImporterForm(forms.Form):

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request', None)
        if 'upload_setup' in kwargs:
            self.upload_setup = kwargs.pop('upload_setup', None)
        super(ModelImporterForm, self).__init__(*args, **kwargs)
