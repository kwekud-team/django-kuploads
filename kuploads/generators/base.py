import inspect
from django.template.defaultfilters import slugify

from kommons.utils.model import get_str_from_model
from kuploads.models import UploadSetup
from kuploads.generators.exporters import ModelExporter
from kuploads.generators.importers import ModelImporter


class ModelExpImp:
    key = 'Regular'
    model = None
    name = None
    exporter_class = ModelExporter
    importer_class = ModelImporter

    def __init__(self, request, site):
        self.request = request

        self.site = site
        self.upload_setup = self.get_upload_setup()
        self.exporter = self.get_exporter_class()(request, self.model, self.upload_setup)
        self.importer = self.get_importer_class()(request, self.model, self.upload_setup)

        if not self.importer.import_fields:
            self.importer.import_fields = self.exporter.get_export_fields()

    def get_exporter_class(self):
        return self.exporter_class

    def get_importer_class(self):
        return self.importer_class

    def get_class_path(self):
        module = inspect.getmodule(self)
        return '%s.%s' % (module.__name__, self.__class__.__name__)

    def get_content_str(self):
        return get_str_from_model(self.model)

    def get_slug(self):
        combined = '%s %s' % (self.get_content_str(), self.key)
        return slugify(combined)

    def get_name(self):
        return self.name or '%s %s' % (self.model._meta.verbose_name, self.key)

    def get_upload_setup(self):
        slug = self.get_slug()
        name = self.get_name()
        content_str = self.get_content_str()
        class_path = self.get_class_path()

        return UploadSetup.objects.update_or_create(
            site=self.site,
            ext_content_str=content_str,
            slug=slug,
            defaults={
                'name': name,
                'class_path': class_path,
            })[0]
