import time
from tablib import Dataset
from tablib.formats import UnsupportedFormat
from kommons.utils.generic import get_sets, intify
from kuploads.data_classes import ImportSheetDataC
from kuploads.constants import KuploadsK


class ModelImporter:
    import_fields = []
    form_class = None
    # issues = []
    # columns_required = [
    #     {'column': 'NAME', 'index': 1},
    # ]

    def __init__(self, request, model, upload_setup):
        self.upload_setup = upload_setup
        self.request = request
        self.model = model

        self._dataset = None
        # self.loader_obj = loader_obj
        # self.sheet_index = sheet_index
        # self.object_defaults = object_defaults

    def get_import_fields(self):
        return self.import_fields

    def get_dataset(self, file_content):
        if self._dataset:
            return self._dataset
        else:
            try:
                self._dataset = Dataset().load(file_content)
                return self._dataset
            except UnsupportedFormat:
                pass

    # TODO: support multiple sheets
    def get_sheets(self, dataset):
        sheets = {}
        if dataset:
            for pos, dset in enumerate([dataset]):
                sheets[pos] = ImportSheetDataC(name=dset.title, sort=pos, row_count=dset.height)
        return sheets

    def run_import(self, upload_file, file_content, sheet_index, progress):
        dataset = self.get_dataset(file_content)
        current_sheet = self.get_sheets(dataset).get(sheet_index, None)
        total = 0
        sets = []
        ceiling = 0

        if current_sheet:
            start_time = time.time()
            pre_run_results = self.pre_run(upload_file, current_sheet)

            dataset = self.get_dataset(file_content)
            total = current_sheet.row_count
            batch_size = upload_file.upload_log.batch_size
            sets = get_sets(total, batch_size)

            floor, ceiling = self.get_boundaries(sets, progress)
            save_results = []
            for pos, obj in enumerate(dataset.dict[floor: ceiling]):
                row_index = floor + pos
                row_data = self.get_field_values(obj)
                res = self.save_commit(upload_file, row_data, row_index, pre_run_results)
                save_results.append(res)

            self.post_run(upload_file, current_sheet, save_results)

            # post process
            end_time = time.time()
            issues = []

            upload_file.item_count = total
            upload_file.time_taken += (end_time - start_time)
            upload_file.issues.update(issues)
            upload_file.save(update_fields=['item_count', 'time_taken', 'issues'])

        return self.get_progress_status(total, sets, progress, ceiling)

    def get_field_values(self, obj):
        # obj is an ordereddict with keys as the headers in sheets. Headers are display only labels that can change
        # anytime. We want to map it to actual field names

        list_values = list(obj.items())

        dt = {}
        for pos, key in enumerate(self.get_import_fields()):
            dt[key] = list_values[pos][1]

        return dt

    def get_progress_status(self, total, sets, progress, ceiling):
        dt = {}
        if total:
            ceiling = (ceiling if ceiling else 1)
            complete_percent = (float(ceiling) / float(total)) * 100
            dt = {
                KuploadsK.IMPORT_PROGRESS_PERCENT_DISPLAY.value: complete_percent,
                KuploadsK.IMPORT_PROGRESS_PERCENT_VALUE.value: intify(complete_percent),
                KuploadsK.IMPORT_PROGRESS_IS_COMPLETED.value: intify(progress) == len(sets),
            }

        return dt

    def get_boundaries(self, sets, progress):
        header = self.upload_setup.structure.get('header', 0)

        s = sets[int(progress) - 1]
        floor, ceiling = s[0], s[1]
        floor = (floor + header if floor == 0 else floor)

        return floor, ceiling

    def save_commit(self, upload_file, row_data, row_index, pre_run_results):
        raise NotImplementedError

    def pre_run(self, upload_file, current_sheet):
        return {}

    def post_run(self, upload_file, current_sheet, save_results):
        return {}

    # def run(self, row_index):
    #     self.issues = []
    #
    #     row = self.loader_obj.read_row(self.sheet_index, row_index)
    #     if row:
    #         is_valid = self.check_valid(row, row_index)
    #         if is_valid:
    #             self.pre_run(row, row_index)
    #             self.prepare(row, row_index)
    #             self.post_run(row, row_index)
    #
    #     return self.issues
    #
    # def pre_run(self, row, row_index):
    #     pass
    #
    # def post_run(self, row, row_index):
    #     pass
    #
    # def prepare(self, row, row_index):
    #     pass
    #
    # def check_valid(self, row, row_index):
    #     # is_empty_row = self.check_required(row, row_index, self.columns_required)
    #     return False if self.issues else True
    #
    # def check_required(self, row, row_index, fields):
    #     empty_vals = []
    #     is_empty_row = False
    #     all_empty = row.count('') == len(row)
    #
    #     if not all_empty:
    #         for x in fields:
    #             value = row[x['index']]
    #             if value:
    #                 empty_vals.append(False)
    #             else:
    #                 empty_vals.append(True)
    #                 all_valid = False
    #                 self.issues.append({
    #                     'row': row_index,
    #                     'type': 'empty',
    #                     'desc': "'%s' is required" % x['column']
    #                 })
    #
    #         if empty_vals.count(True) == len(fields):
    #             # This means all required fields were not entered. This is treated
    #             # as an empty line and not uploaded
    #             is_empty_row = True
    #
    #     return is_empty_row
