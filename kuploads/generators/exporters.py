import tablib
from django.http import HttpResponse
from django.template.defaultfilters import slugify, strip_tags

from vproxy.utils import VProxyUtils
from kommons.utils.core import dotted_import
from kuploads.constants import KuploadsK


class ModelExporter:
    export_fields = []
    sample_data = []
    ignore_fields = []
    sample_queryset_limit = None
    queryset_limit = None

    def __init__(self, request, model, upload_setup):
        self.upload_setup = upload_setup
        self.request = request
        self.model = model

    def get_class(self):
        if self.upload_setup.class_path:
            return dotted_import(self.upload_setup.class_path)
        raise Exception('cls not provided')

    def get_export_fields(self):
        export_fields = self.export_fields
        ignore_fields = self.ignore_fields
        return [x for x in export_fields if x not in ignore_fields]

    def get_headers(self, vproxy_dict=None):
        xs = VProxyUtils(self.request).get_vproxy_fields(self.model, self.get_export_fields(), vproxy_dict=vproxy_dict)
        return [x.label.upper() for x in xs]

    def get_data(self, ttype, queryset=None):
        data = []
        if ttype == KuploadsK.EXPORT_TYPE_SAMPLE.value:
            if self.sample_queryset_limit and queryset:
                data = self.get_queryset_data(queryset, self.sample_queryset_limit)
            elif self.sample_data:
                data = self.sample_data
        else:
            if queryset:
                data = self.get_queryset_data(queryset, self.queryset_limit)

        return data

    def get_queryset_data(self, queryset, limit=None):
        data = []

        for obj in queryset[:limit]:
            tmp = []
            for field in self.get_export_fields():
                val = self.get_field_val(obj, field)

                val = val or ''
                tmp.append(strip_tags(val))
            data.append(tmp)

        return data

    def get_queryset(self, queryset):
        return queryset

    def get_field_val(self, obj, field):
        attr = getattr(obj, field)
        if callable(attr):
            val = attr()
        else:
            val = attr
        return val

    def get_title(self):
        return str(self.model._meta.verbose_name_plural).upper()

    def get_export_response(self, fformat, ttype='', queryset=None):
        headers = self.get_headers()
        queryset = self.get_queryset(queryset)
        data = self.get_data(ttype, queryset=queryset)

        dataset = tablib.Dataset(title=self.get_title(), headers=headers)
        dataset.extend(data)

        content_type = 'application/%s' % fformat
        filename = slugify(f'{self.upload_setup.name} {ttype}')
        response = HttpResponse(getattr(dataset, fformat), content_type=content_type)
        response['Content-Disposition'] = f'attachment; filename={filename}.{fformat}'
        return response
