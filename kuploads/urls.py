from django.urls import path

from kuploads import views

app_name = 'kuploads'


urlpatterns = [
    path('exporter/<int:up_setup_pk>/', views.ModelExporterView.as_view(), name='model_exporter'),
    path('form/<int:site_id>/<str:ext_content_str>/<str:slug>/', views.UploadFormView.as_view(), name='upload_form'),
    path('form/<int:up_setup_pk>/', views.UploadFormView.as_view(), name='upload_form'),
    # path(r'^main/(?P<pk>.+)/$', UploadSetupView.as_view(), name='main'),
    path('progress/<int:pk>/', views.UploadProgressView.as_view(), name='progress'),
    path('complete/<int:pk>/', views.UploadCompleteView.as_view(), name='complete'),
    # path(r'^verify/(?P<pk>.+)/$', UploadVerifyView.as_view(), name='verify'),
    path('generator/<int:file_pk>/<int:sheet_index>/', views.UploadGeneratorView.as_view(), name='generator'),
    # path(r'^complete/(?P<pk>.+)/$', 'upload_complete', name='complete'),
    #
    # path(r'^duplicate-template/(?P<pk>.+)/$', 'duplicate_upload_template', name="duplicate-template"),
    # path(r'^structure/json/$', 'json_upload_structure', name='structure'),
    # path(r'^templates/json/$', 'json_related_templates', name="related-templates"),
    # path('sample-fields/json/', views.json_show_sample_fields, name="sample_fields"),
    # (r'^upload/prepare/$', 'prepare_upload'),
]
